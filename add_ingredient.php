<?php

require 'connexion.php';

$req = $bdd->prepare('SELECT * FROM ingredients');
$req->execute();
$ingredients = $req->fetchAll();



if (!empty( $_POST['nameIngredient'] )) {
    $nameIngredient = $_POST['nameIngredient'];

    $query = $bdd->prepare( "INSERT INTO ingredients (nom_ingredient) VALUES(?)" );
    $query->execute( array($nameIngredient) );

}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LEs Recettes PAS Pompettes</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</head>
<body style="text-align: center">
<?php
include 'header.php';
?>

<form method="post" action="#" style="width: 75%; margin-left: 12.5%">
    <h1>Ajouter un ingrédient
        <?php
        if (empty($_SESSION) && !empty($_POST)){
        ?>
        (Connectez-vous)</h1>
    <?php
    }else{
        echo "</h1>";
    }
    ?>
    <div class="mb-3">
        <label class="form-label">Nom de l'ingrédient</label>
        <input type="text" class="form-control" name="nameIngredient" required>
    </div>

    <button type="submit" class="btn btn-primary">Ajouter un ingrédient</button>
    <?php
    if (empty($_SESSION)){
        ?>
        <br>
        <br>
        <h1>Veuillez vous connecter pour ajouter une recette</h1>
        <?php
    }
    ?>
</form>
</body>
</html>
