<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Les Recettes PAS Pompettes</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</head>
<body>
<?php
include 'header.php';
?>
<div style="display: flex; flex-flow: row wrap; text-align: center">

    <?php

    require 'connexion.php';

    $req = $bdd->prepare('SELECT * FROM recette');
    $req->execute();
    $liste = $req->fetchAll();

    foreach ($liste as $element) {
        ?>
        <div class="card" style="width: 18rem; margin: 20px">
            <div class="card-body">
                <h5 class="card-title"><?= $element['nom_recette'] ?></h5>
                <p class="card-text">Cliquez pour plus d'information :</p>
                <a href="./recette.php?recette=<?= $element['id'] ?>" class="btn btn-primary">Consulter</a>
            </div>
        </div>
        <?php
    }

    ?>
</div>
</body>
</html>
