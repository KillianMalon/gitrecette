<?php

require 'connexion.php';

$req = $bdd->prepare('SELECT * FROM ingredients');
$req->execute();
$ingredients = $req->fetchAll();



if (!empty( $_POST['namePlat'] ) && !empty( $_POST['description'] ) && !empty( $_POST['time'] )) {
    $namePlat = $_POST['namePlat'];
    $description = $_POST['description'];
    $time = $_POST['time'];
    $firstIngredient = intval( $_POST['firstIngredient'] );
    $secondIngredient = intval( $_POST['secondIngredient'] );
    $thirdIngredient = intval( $_POST['thirdIngredient'] );
    $fourthIngredient = intval( $_POST['fourthIngredient'] );
    $fivthIngredient = intval( $_POST['fivthIngredient'] );

    $ingredient = array();

    if ($firstIngredient === "") {
        $firstIngredient = 0;
    }
    if ($secondIngredient === "") {
        $secondIngredient = 0;
    }
    if ($thirdIngredient === "") {
        $thirdIngredient = 0;
    }
    if ($fourthIngredient === "") {
        $fourthIngredient = 0;
    }
    if ($fivthIngredient === "") {
        $fivthIngredient = 0;
    }


    array_push( $ingredient, $firstIngredient );
    array_push( $ingredient, $secondIngredient );
    array_push( $ingredient, $thirdIngredient );
    array_push( $ingredient, $fourthIngredient );
    array_push( $ingredient, $fivthIngredient );

    var_dump($ingredient);

    $query = $bdd->prepare( "INSERT INTO recette (nom_recette, auteur_recette, temps_recette, description_recette) VALUES(?, 1 , ? , ?)" );
    $query->execute( array($namePlat, $time, $description) );

    $query = $bdd->prepare( "SELECT id FROM recette WHERE nom_recette = ?" );
    $query->execute( array($namePlat) ); // execute le SQL dans la base de données (MySQL / MariaDB)
    $id = $query->fetch()[0];

    foreach ($ingredient as $ingr) {

        if ($ingr != 0) {
            $query = $bdd->prepare( "INSERT INTO ingredient_recette (id_recette, id_ingredient) VALUES( ?,?)" );
            $query->execute( array($id, $ingr) );
        }
    }

}

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LEs Recettes PAS Pompettes</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</head>
<body style="text-align: center">
<?php
include 'header.php';
?>

<form method="post" action="#" style="width: 75%; margin-left: 12.5%">
    <h1>Ajouter une recette
        <?php
        if (empty($_SESSION) && !empty($_POST)){
        ?>
        (Connectez-vous)</h1>
    <?php
    }else{
        echo "</h1>";
    }
    ?>
    <div class="mb-3">
        <label class="form-label">Nom du plat</label>
        <input type="text" class="form-control" name="namePlat" required>
    </div>
    <div class="mb-3" style="display: flex; flex-direction: column; height: 60vh">
        <label class="form-label">Recette</label>
        <!--            <input type="textarea" class="form-control" id="recette">-->
        <textarea style="height: 100%" name="description"></textarea>
    </div>
    <div style="margin-bottom: 15px; display: flex; flex-direction: row; justify-content: space-between">
        <select name="firstIngredient">
            <option value="">Ingrédients</option>
            <?php
            foreach($ingredients as $ingredient){

                $idIngredient = $ingredient['id'];
                $nameIngredient = $ingredient['nom_ingredient'];
                ?>
                <option value="<?php echo $idIngredient?>"><?php echo $nameIngredient; ?></option>
                <?php
            }
            ?>

        </select>
        <select name="secondIngredient">
            <option value="">Ingrédients</option>
            <?php
            foreach($ingredients as $ingredient){

                $idIngredient = $ingredient['id'];
                $nameIngredient = $ingredient['nom_ingredient'];
                ?>
                <option value="<?php echo $idIngredient?>"><?php echo $nameIngredient; ?></option>
                <?php
            }
            ?>
        </select>
        <select name="thirdIngredient">
            <option value="">Ingrédients</option>
            <?php
            foreach($ingredients as $ingredient){

                $idIngredient = $ingredient['id'];
                $nameIngredient = $ingredient['nom_ingredient'];
                ?>
                <option value="<?php echo $idIngredient?>"><?php echo $nameIngredient; ?></option>
                <?php
            }
            ?>
        </select>
        <select name="fourthIngredient">
            <option value="">Ingrédients</option>
            <?php
            foreach($ingredients as $ingredient){

                $idIngredient = $ingredient['id'];
                $nameIngredient = $ingredient['nom_ingredient'];
                ?>
                <option value="<?php echo $idIngredient?>"><?php echo $nameIngredient; ?></option>
                <?php
            }
            ?>
        </select>
        <select name="fivthIngredient">
            <option value="">Ingrédients</option>
            <?php
            foreach($ingredients as $ingredient){

                $idIngredient = $ingredient['id'];
                $nameIngredient = $ingredient['nom_ingredient'];
                ?>
                <option value="<?php echo $idIngredient?>"><?php echo $nameIngredient; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class="mb-3">
        <label class="form-label">Temps de la recette (minutes)</label>
        <input type="number" class="form-control" name="time" required>
    </div>


    <button type="submit" class="btn btn-primary">Ajouter recette</button>
    <?php
    if (empty($_SESSION)){
        ?>
        <br>
        <br>
        <h1>Veuillez vous connecter pour ajouter une recette</h1>
        <?php
    }
    ?>
</form>



</body>
</html><?php
?>
<script>
    let ajouter = document.querySelector()
</script>
