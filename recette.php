<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Les Recettes PAS Pompettes</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</head>
<body>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include 'header.php';
?>
<div style="display: flex; flex-flow: column wrap; text-align: center">
    <?php
    require 'connexion.php';

    if (isset($_GET['recette']) && !empty($_GET['recette'])){

        $idrecette = $_GET['recette'];

        $req = $bdd->prepare('SELECT * FROM recette WHERE id = :id');
        $req->execute(array(
            "id" => $idrecette
        ));
        $recettes = $req->fetchAll();

        if (!empty($recettes)){
            foreach ($recettes as $recette){

                $aut = $bdd->prepare('SELECT * FROM auteurs WHERE id = :ida');
                $aut->execute(array(
                    "ida" => $recette['auteur_recette']
                ));
                $auteurs = $aut->fetchAll();
                $recette_auteur = '';
                foreach ($auteurs as $auteur){
                    $recette_auteur = $auteur['nom']. ' '. $auteur['prenom'];
                }
                ?>

                <h1><?=$recette['nom_recette']?></h1>
                <br><br>
                <img src="./assets/<?=$recette['photo_recette']?>" width="400px">
                <br>
                <h3>Temps de cuisson : <?= $recette['temps_recette'] ?> minutes</h3>
                <br><br>
                <h4>
                    <?= $recette['decription_recette'] ?>
                </h4>
                <br><br>
                <h3>Auteur : <?= $recette_auteur ?></h3>
                <?php
            }
        }
    }
    ?>
</div>
</body>
</html>
